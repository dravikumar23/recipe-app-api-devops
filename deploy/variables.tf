variable "prefix" {
  type    = string
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"

}

variable "contact" {
  default = "ravikumard@outlook.com"
}